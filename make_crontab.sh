#!/usr/bin/env bash

YML_FILE="/home/kevin/linode-server-setup/nginx/docker-compose.yml"

(crontab -l 2>/dev/null; \
    echo "@daily certbot renew --pre-hook 'docker-compose -f ${YML_FILE} down' --post-hook 'docker-compose -f ${YML_FILE} up -d'") |\
    crontab -
