#!/usr/bin/env bash

# Works on Ubuntu servers on Linode
# Lates: 20.10

SERVER_URL="SERVER-URL"
EMAILADDR=""
ORIG_PWD=$PWD

sudo apt update && apt upgrade -y

# -------------------------------------------------------------------------------
# installing docker -------------------------------------------------------------
# -------------------------------------------------------------------------------
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update -y

sudo apt install -y docker-ce docker-ce-cli containerd.io

# testing using: docker run hello-world

# -------------------------------------------------------------------------------
# installing docker-compose -----------------------------------------------------
# -------------------------------------------------------------------------------
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose


# -------------------------------------------------------------------------------
# building the jupyterhub image -------------------------------------------------
# -------------------------------------------------------------------------------
sudo docker build -t kleiss/jupyterhub:latest jupyterhub/.

# cd nginx
# mkdir www
# cd ..

# back to home

# -------------------------------------------------------------------------------
# installing certbot ------------------------------------------------------------
# -------------------------------------------------------------------------------
sudo snap install core && sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

sudo certbot certonly \
    --standalone \
    -d $SERVER_URL \
    -m $EMAILADDR \
    --agree-tos

cd $ORIG_PWD
sudo ./make_crontab.sh


# -------------------------------------------------------------------------------
# prepare nginx server config file ----------------------------------------------
# -------------------------------------------------------------------------------
sed -e "s/{URL}/${SERVER_URL}/" nginx/nginx.blank > nginx/nginx.conf
